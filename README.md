this is a simple simulation of the evolution theory done on data structures representing the biological dna. the results of this script shows the shift of this data structures towords speciation.  
a detailed explanation of the script and what its purpose can be found [here](./evolve.md).
